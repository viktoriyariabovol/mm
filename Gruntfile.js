/*global module:false*/
module.exports = function(grunt) {

  grunt.initConfig({
    jshint: {
      files: ['Gruntfile.js', 'src/js/app/**/*.js', 'src/js/app/*.js'],
          // options: {
          // curly: true,
          // eqeqeq: true,
          // immed: true,
          // latedef: true,
          // newcap: true,
          // noarg: true,
          // sub: true,
          // undef: true,
          // boss: true,
          // eqnull: true,
          // browser: true,
          // globals: {
          // require: true,
          // define: true,
          // requirejs: true,
          // describe: true,
          // expect: true,
          // it: true
          // }
          // }
    },

   compass: {
    dist: {
      options: {
        config: 'config.rb',
        require: 'susy'

      }
    }
   },

    uglify: {
      app: {
        files: {
          'assets/js/app.min.js': 'assets/js/app.js'
        }
      },
      
      foundation: {
        files: {
          'assets/js/foundation.min.js': 'assets/js/foundation.js'
        }
      },

      vendor: {
        files: {
          'assets/js/vendor.min.js': 'assets/js/vendor.js'
        }
      }
    },

    "merge-json": {
        dat: {
            src: "src/js/data/*.json",
            dest: "assets/js/data.json",
        }    
    },

   concat: {   
      app: {
        options: {
          separator: ';'
        },
        src: [
          'src/js/app/*.js'
        ],
        dest: 'assets/js/app.js'
      },
      foundation: {
        options: {
          separator: ';'
        },
        src: [
          'src/js/foundation/foundation.js',
          'src/js/foundation/*.js'
        ],
        dest: 'assets/js/foundation.js'
      },
      vendor: {
        options: {
          separator: ';'
        },
        src: [
          'src/js/vendor/jquery.js',
          'src/js/vendor/jquery.autocomplete.js',
          'src/js/vendor/jquery.cookie.js',
          'src/js/vendor/jquery.fittext.js',
          'src/js/vendor/jquery.ui.widget.js',
          'src/js/vendor/*.js'
        ],
        dest: 'assets/js/vendor.js'
      },
    },

    watch: {
      jshint: {
          files: ['Gruntfile.js', 'src/js/app/**/*.js', 'src/js/app/*.js'],
          tasks: ['jshint', 'concat', 'uglify']
         // options: {
          //  livereload: true
          //}
      },
      foundation: {
          files: ['src/scss/**/*.scss', 'src/scss/*.scss'],
          tasks: ['compass'],
          //environment: 'development',
          //options: {
           // livereload: true
          //}
      }
    }

    


  });

 
  // Load JSHint task
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-merge-json');

  // Default task.
  grunt.registerTask('default', 'jshint', 'compass', 'concat');


};
