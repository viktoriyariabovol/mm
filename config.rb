preferred_syntax = :scss
http_path = '/'
css_dir = 'assets/css'
sass_dir = 'src/scss'
images_dir = 'assets/img'
relative_assets = true
line_comments = true
# output_style = :compressed
