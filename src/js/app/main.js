var main = '#0066cc';
var bg = '#ffffff';
var grey = '#e1e1e1';
var options = '#cccccc';


function colorizeMain(main) {
  $('.content-header, .side-nav').css ({
    backgroundColor: main
  });

  $('.side-nav li:first-child, li:last-child, .back-arrow').css({
    backgroundColor: $.xcolor.darken(main)
  });

   $('.header-nav thead, .header-nav tr, .header-nav th, .header-nav h2').css({
      backgroundColor: main
   });
}


$( document ).ready(function() {
  colorizeMain(main);

  $('#main-color').change(function(){
    colorizeMain($(this).val());
  });

  $('html, body').css ({
    backgroundColor: grey,
    color: '#999999'
  });

  $('footer, header').css ({
    backgroundColor: bg
  });

  $('.content').css ({
    backgroundColor: bg
  });

  $('.options-content').css ({
    backgroundColor: $.xcolor.lighten(options)
  });

  $('.options-content tr:nth-child(even)').css ({
    backgroundColor: options
  });
 

});  